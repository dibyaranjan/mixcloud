"""
File:           mixcloud.py
Author:         Dibyaranjan Sathua
Created on:     10/09/20, 11:03 PM
"""
import os

import requests
import yaml
from dateutil import tz
import datetime
import colorama

from src.constants import Constants, PayloadKey, YamlKey
from src.access_token import AccessToken
from src.exception import YamlFileFormatException, AccessTokenException
from src.config_parser import ConfigParser


class Section:
    """ Tracklist information """
    def __init__(self, artist, song, chapter, start_time):
        self.artist = artist
        self.song = song
        self.chapter = chapter
        self.start_time = start_time


class Cloudcast:
    """ Store cloudcast """
    def __init__(self, mp3, name, picture, description, tags, publish_date, sections,
                 disable_comments, unlisted, hide_stats, date):
        self.mp3 = mp3
        self.name = name
        self.picture = picture
        self.description = description
        self.tags = tags
        self.publish_date = publish_date
        self.sections = [
            Section(
                artist=x.get(YamlKey.ARTIST),
                song=x.get(YamlKey.SONG),
                chapter=x.get(YamlKey.CHAPTER),
                start_time=x.get(YamlKey.START_TIME)
            )
            for x in sections
        ]
        self.disable_comments = disable_comments
        self.unlisted = unlisted
        self.hide_stats = hide_stats
        self.date = self.local_time_to_utc(date) if date else date

    @staticmethod
    def local_time_to_utc(date_str):
        """ Convert local date string to UTC string """
        utc_zone = tz.tzutc()
        local_zone = tz.tzlocal()

        # Convert time string to datetime
        local_time = datetime.datetime.strptime(date_str, "%Y-%m-%d %H:%M")

        # Tell the datetime object that it's in local time zone since
        # datetime objects are 'naive' by default
        local_time = local_time.replace(tzinfo=local_zone)
        # Convert time to UTC
        utc_time = local_time.astimezone(utc_zone)
        # Generate UTC time string (YYYY-MM-DDTHH:MM:SSZ )
        utc_string = utc_time.strftime("%Y-%m-%dT%H:%M:%SZ")
        return utc_string

    @classmethod
    def from_yaml(cls, yaml_file):
        parser = ConfigParser(yaml_file)
        parser.parse()
        # with open(yaml_file, mode="r") as infile:
        #     yaml_data = yaml.full_load(infile)
        yaml_data = parser.yaml_data

        if YamlKey.MP3 not in yaml_data:
            raise YamlFileFormatException("MP3 is missing")
        if YamlKey.NAME not in yaml_data:
            raise YamlFileFormatException("Name is missing")

        mp3 = yaml_data.get(YamlKey.MP3)
        name = yaml_data.get(YamlKey.NAME)
        picture = yaml_data.get(YamlKey.PICTURE)
        description = parser.description
        tags = yaml_data.get(YamlKey.TAGS, [])
        publish_date = yaml_data.get(YamlKey.PUBLISH_DATE)
        sections = yaml_data.get(YamlKey.TRACKS, [])
        disable_comments = yaml_data.get(YamlKey.COMMENTS, False)
        unlisted = yaml_data.get(YamlKey.UNLISTED, False)
        hide_stats = yaml_data.get(YamlKey.HIDE_STATS, False)
        date = yaml_data.get(YamlKey.PUBLISH_DATE)

        # Check if files exists
        if not os.path.isfile(mp3):
            raise YamlFileFormatException(f"MP3 file {mp3} does not exist")

        if picture and not os.path.isfile(picture):
            raise YamlFileFormatException(f"Picture file {picture} does not exist")

        # Check for no of characters in description
        if len(description) > Constants.MAX_DESCRIPTION_CHARS:
            raise YamlFileFormatException(f"Maximum {Constants.MAX_DESCRIPTION_CHARS} "
                                          f"is allowed in description. You have {len(description)} "
                                          f"characters.")

        # check file size
        file_stats = os.stat(mp3)
        if file_stats.st_size > Constants.MP3_MAX_SIZE:
            raise YamlFileFormatException(
                f"MP3 file size {file_stats.st_size} is more than allowed "
                f"size {Constants.MP3_MAX_SIZE}"
            )

        if picture:
            file_stats = os.stat(picture)
            if file_stats.st_size > Constants.PICTURE_MAX_SIZE:
                raise YamlFileFormatException(
                    f"Picture file size {file_stats.st_size} is more than allowed "
                    f"size {Constants.PICTURE_MAX_SIZE}"
                )

        return cls(mp3=mp3, name=name, picture=picture, description=description, tags=tags,
                   publish_date=publish_date, sections=sections, disable_comments=disable_comments,
                   unlisted=unlisted, hide_stats=hide_stats, date=date)


class Mixcloud:
    """ Class to upload mp3 files to your account """

    def __init__(self, api_root=Constants.API_ROOT, access_token=None):
        self.api_root = api_root
        self.access_token = access_token
        if access_token is None:
            self.access_token = AccessToken.read()

    def _validate_access_token(self):
        """ This will validate the access token """
        url = f"{self.api_root}/me/"
        response = requests.get(
            url,
            params={'access_token': self.access_token},
        )
        if response.ok:
            print(f"{colorama.Fore.BLUE}Access token validated successfully.")
        else:
            print(f"{colorama.Fore.RED}Invalid access token")
            raise AccessTokenException("Invalid access token")

    def upload(self, cloudcast):
        """ Upload the data using API """
        url = f"{self.api_root}/upload/"
        files = {}
        payload = {
            PayloadKey.NAME: cloudcast.name,
        }
        if cloudcast.description is not None:
            payload[PayloadKey.DESCRIPTION] = cloudcast.description

        if cloudcast.picture is not None:
            files = {PayloadKey.PICTURE: cloudcast.picture}

        if cloudcast.disable_comments:
            payload[PayloadKey.COMMENTS] = True

        if cloudcast.unlisted:
            payload[PayloadKey.UNLISTED] = True

        if cloudcast.hide_stats:
            payload[PayloadKey.HIDE_STATS] = True

        if cloudcast.date:
            payload[PayloadKey.PUBLISH_DATE] = cloudcast.date

        for num, sec in enumerate(cloudcast.sections):
            if sec.artist:
                payload[PayloadKey.ARTIST.format(num)] = sec.artist
            if sec.song:
                payload[PayloadKey.SONG.format(num)] = sec.song
            if sec.chapter:
                payload[PayloadKey.CHAPTER.format(num)] = sec.chapter
            if sec.start_time:
                payload[PayloadKey.START_TIME.format(num)] = sec.start_time

        for num, tag in enumerate(cloudcast.tags):
            payload[PayloadKey.TAGS.format(num)] = tag

        mp3file = None
        picture_file = None

        try:
            mp3file = open(cloudcast.mp3, mode="rb")
            files[PayloadKey.MP3] = mp3file
            if cloudcast.picture is not None:
                picture_file = open(cloudcast.picture, mode="rb")
                files[PayloadKey.PICTURE] = picture_file

            # Both the file handlers need to be open while sending the POST request
            response = requests.post(
                url,
                data=payload,
                params={'access_token': self.access_token},
                files=files
            )
            return response
        except:
            pass
        finally:
            if mp3file:
                mp3file.close()
            if picture_file:
                picture_file.close()

    def upload_from_yaml(self, yaml_file):
        if not os.path.isfile(yaml_file):
            print(f"{colorama.Fore.RED}Input YAML file does not exist")
        # Check if the access token is valid
        self._validate_access_token()
        cloudcast = Cloudcast.from_yaml(yaml_file)
        response = self.upload(cloudcast)
        return response
