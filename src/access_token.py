"""
File:           access_token.py
Author:         Dibyaranjan Sathua
Created on:     10/09/20, 11:13 PM
"""
import os

from src.constants import Constants


class AccessToken:
    """ Read and write access token from file """
    TOKEN_FILE = os.path.join(os.path.expanduser("~"), Constants.ACCESS_TOKEN_FILE)

    @staticmethod
    def write(access_token):
        """ Save access token to .sathualab_mixcloud_api file present in your home directory """
        with open(AccessToken.TOKEN_FILE, mode="w") as outfile:
            outfile.write(f"{access_token}\n")

    @staticmethod
    def read():
        """ Read access token from staticmethod """
        with open(AccessToken.TOKEN_FILE, mode="r") as infile:
            line = infile.readline().strip()
        return line
