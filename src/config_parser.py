"""
File:           config_parser.py
Author:         Dibyaranjan Sathua
Created on:     01/10/20, 1:44 AM
"""
import re
import yaml


class ConfigParser:
    """ Custom parser for config file """

    def __init__(self, config_file):
        self._config_file = config_file
        self._description_regex = re.compile(r"\[description\]")
        self._end_regex = re.compile(r"\[end\]")
        self._description = ""
        self._yaml_data = dict()

    def parse(self):
        """ Parse function """
        yaml_parsing_string = ""
        flag = False
        with open(self._config_file, mode="r") as infile:
            for line in infile:
                match_str = self._description_regex.search(line)
                if match_str:
                    flag = True
                    continue

                match_str = self._end_regex.search(line)
                if match_str:
                    flag = False
                    continue

                if flag:
                    self._description += line
                    continue

                yaml_parsing_string += line

        self._yaml_data = yaml.safe_load(yaml_parsing_string)

    @property
    def description(self):
        return self._description

    @property
    def yaml_data(self):
        return self._yaml_data


if __name__ == "__main__":
    file = "/home/dibyaranjan/Downloads/HHH_272_modified_yaml_enhanced.yaml"
    obj = ConfigParser(file)
    obj.parse()



