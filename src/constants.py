"""
File:           constants.py
Author:         Dibyaranjan Sathua
Created on:     10/09/20, 9:17 PM
"""


class Constants:
    """ This class holds different constants used by different constants """
    ACCESS_TOKEN_FILE = ".sathualab_mixcloud_api"
    API_ROOT = "https://api.mixcloud.com"
    OAUTH_ROOT = "https://www.mixcloud.com/oauth"
    MP3_MAX_SIZE = 4294967296
    PICTURE_MAX_SIZE = 10485760
    MAX_DESCRIPTION_CHARS = 1000


class PayloadKey:
    """ Keys for upload payload """
    MP3 = "mp3"
    NAME = "name"
    DESCRIPTION = "description"
    PICTURE = "picture"
    TAGS = "tags-{}-tag"
    PUBLISH_DATE = "publish_date"
    ARTIST = "sections-{}-artist"
    SONG = "sections-{}-song"
    CHAPTER = "sections-{}-chapter"
    START_TIME = "sections-{}-start_time"
    COMMENTS = "disable_comments"
    UNLISTED = "unlisted"
    HIDE_STATS = "hide_stats"


class YamlKey:
    """ Keys for yaml config """
    MP3 = "mp3"
    NAME = "name"
    DESCRIPTION = "description"
    PICTURE = "picture"
    TAGS = "tags"
    PUBLISH_DATE = "date"
    TRACKS = "tracks"
    START_TIME = "time"
    ARTIST = "artist"
    SONG = "song"
    CHAPTER = "chapter"
    COMMENTS = "disable comments"
    UNLISTED = "unlisted"
    HIDE_STATS = "hide stats"

