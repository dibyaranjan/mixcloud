"""
File:           authentication.py
Author:         Dibyaranjan Sathua
Created on:     10/09/20, 9:12 PM

Generate access token for your app.
"""
import requests
from urllib.parse import urlencode

from src.exception import MixcloudOAuthException
from src.constants import Constants


class MixcloudOAuth:
    """ OAuth class for authentication to get access token """

    def __init__(self, client_id=None, client_secret=None, redirect_uri=None):
        self.client_id = client_id
        self.client_secret = client_secret
        self.redirect_uri = redirect_uri

    def authorize_url(self):
        """
        Return a URL to redirect the user to for OAuth authentication.
        """
        auth_url = f"{Constants.OAUTH_ROOT}/authorize"
        params = {
            "client_id": self.client_id,
            "redirect_uri": self.redirect_uri,
        }
        return f"{auth_url}?{urlencode(params)}"

    def exchange_token(self, code):
        """
        Exchange the authorization code for an access token.
        """
        access_token_url = f"{Constants.OAUTH_ROOT}/access_token"
        params = {
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'redirect_uri': self.redirect_uri,
            'code': code,
        }
        resp = requests.get(access_token_url, params=params)
        if not resp.ok:
            raise MixcloudOAuthException("Could not get access token.")
        return resp.json()['access_token']
