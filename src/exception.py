"""
File:           exception.py
Author:         Dibyaranjan Sathua
Created on:     10/09/20, 9:15 PM

Generic exceptions
"""


class MixcloudOAuthException(Exception):
    pass


class YamlFileFormatException(Exception):
    pass


class AccessTokenException(Exception):
    pass
