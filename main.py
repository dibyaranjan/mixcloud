#!/usr/bin/env python3
"""
File:           main.py
Author:         Dibyaranjan Sathua
Created on:     11/09/20, 2:50 AM

Code by: SathuaLab (sathualab.com)
Email: dibyaranjan.sathua@gmail.com
"""
import sys
from urllib.parse import urljoin

import colorama

from src import MixcloudOAuth, AccessToken, Mixcloud, YamlFileFormatException, AccessTokenException

BASE_URL = "https://www.mixcloud.com/"


def upload(yaml_file):
    """ Upload shows using yaml file """
    print(f"\n\t{colorama.Fore.GREEN}Uploading... Please wait")
    mixcloud = Mixcloud()
    try:
        response = mixcloud.upload_from_yaml(yaml_file)
    except (YamlFileFormatException, AccessTokenException) as err:
        print(f"{colorama.Fore.RED}ERROR !!! {err}")
        print(f"{colorama.Fore.RESET}")
        sys.exit(1)

    output = response.json()
    print(f"{colorama.Fore.CYAN}{output}")
    if response.ok:
        try:
            shows_url = urljoin(BASE_URL, output["result"]["key"])
            print(f"\n\t{colorama.Fore.CYAN}URL: {shows_url}")
        except KeyError:
            pass
        print(f"\n\t{colorama.Fore.BLUE} Uploaded successfully")
    else:
        print(f"\n\t{colorama.Fore.RED}Uploading failed")


def main():
    """ Main function """
    print(f"\n\t\t{colorama.Fore.BLUE}################### Code by SathuLab ######################")
    # Check if the YAM file is passed through command line arguments
    if len(sys.argv) == 2:
        yaml_file = sys.argv[1]
        upload(yaml_file)
        return None

    # Else Prompt console menu
    print(f"{colorama.Fore.GREEN}1. Input YAML file")
    print(f"{colorama.Fore.GREEN}2. Generate Access token")
    print(f"{colorama.Fore.LIGHTGREEN_EX}Press any other key to exit")
    choice = int(input(f"{colorama.Fore.BLUE}Enter your choice: "))

    if choice == 1:
        yaml_file = input(f"\n\t{colorama.Fore.LIGHTMAGENTA_EX}YAML File: ")
        upload(yaml_file)

    elif choice == 2:
        client_id = input(f"\n\t{colorama.Fore.LIGHTMAGENTA_EX}Client ID: ")
        client_secret = input(f"\t{colorama.Fore.LIGHTMAGENTA_EX}Client Secret: ")
        # redirect_uri = "https://sathualab.com"
        redirect_uri = "https://PariahRocks.com"
        auth = MixcloudOAuth(client_id, client_secret, redirect_uri)
        print("Copy below URL in your browser to generate the Oauth code")
        print(auth.authorize_url())
        oauth_code = input(f"\n\t{colorama.Fore.LIGHTMAGENTA_EX}Oauth Code: ")
        access_token = auth.exchange_token(oauth_code)
        AccessToken.write(access_token)
        print(f"\n\t\t{colorama.Fore.RED} Access Token: {access_token}")

    print(f"{colorama.Fore.RESET}")


if __name__ == "__main__":
    main()
