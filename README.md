# mixcloud

Upload MP3 and other information to mixcloud account using API.
API: https://www.mixcloud.com/developers/

Steps:
1. Download and install python3.7 from https://www.python.org/ftp/python/3.7.5/python-3.7.5-macosx10.9.pkg
2. Download pip3 from https://bootstrap.pypa.io/get-pip.py
3. Install pip3 using "python3 get-pip.py"
4. pip3 install -r requirements.txt

There are two different ways to run the script.
1. Console mode (./main.py)
2. Command line arguments (./main.py <yaml-file>)

Before running the scripts, you have to generate the Access Token. You can run the script using
console mode and follow the step by step procedure to generate the access token. This is a one
time process.
